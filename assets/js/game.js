/**
* Created with comp307.
* User: devnetf
* Date: 2015-01-30
* Time: 02:59 AM
* To change this template use Tools | Templates.
*/

function CardUI(name, src)
{
    this.name = name;
    this.bitmap = new createjs.Bitmap(src).set({x:370,y:480});
    this.hoverImage = new createjs.Bitmap(src);
    this.moving = false;
    var offset = {};
    var that = this;

    var drag = function(evt) {
        
        if(!that.moving)
        {
            //this run only once
            offset.x = evt.stageX - this.x;
            offset.y = evt.stageY - this.y;
        }
    
        that.moving = true;
        
        evt.currentTarget.x = evt.stageX - offset.x;
        evt.currentTarget.y = evt.stageY - offset.y;
        evt.currentTarget.rotation = 0; 
    };

    this.enableDrag = function ()
    {
         
        this.bitmap.on("pressmove", drag);
         
        this.bitmap.on("pressup", function(evt) { that.moving = false; });
    }

    this.disableDrag = function ()
    {    
        this.bitmap.off("pressmove", drag);
    }
}

/*@param 
 * point: the point we want to rotate
 * center: axe of rotation
 * angle: angle to rotate in degree
 */
function rotatePoint(point, center, angle){
    angle = (angle ) * (Math.PI/180); // Convert to radians
    var rotatedX = Math.cos(angle) * (point.x - center.x) - Math.sin(angle) * (point.y-center.y) + center.x;
    var rotatedY = Math.sin(angle) * (point.x - center.x) + Math.cos(angle) * (point.y - center.y) + center.y;

    return new createjs.Point(rotatedX,rotatedY);
}

hsbuilder.controller('gameController', function($scope, $http, $rootScope) {

    $scope.stage = new createjs.Stage("hearthstone");

    $scope.selectedCards=[
                          new CardUI("ragnaros", "/assets/img/cards/missing.png"),
                          new CardUI("ragnaros", "/assets/img/cards/cardback.png"), 
                          new CardUI("ragnaros", "/assets/img/cards/missing.png"), 
                          new CardUI("ragnaros", "/assets/img/cards/cardback.png"),
                          new CardUI("ragnaros", "/assets/img/cards/missing.png"), 
                          new CardUI("ragnaros", "/assets/img/cards/cardback.png"),
                          new CardUI("ragnaros", "/assets/img/cards/missing.png"), 
                          new CardUI("ragnaros", "/assets/img/cards/cardback.png"),
                          new CardUI("ragnaros", "/assets/img/cards/missing.png"), 
                          new CardUI("ragnaros", "/assets/img/cards/cardback.png")
                          ];
     
    $scope.init = function()
    {
        $scope.stage.enableMouseOver();
        var map = new createjs.Bitmap("/assets/img/maps/naxxramas.jpg");
        $scope.stage.addChild(map); 

        var numCards = $scope.selectedCards.length;
            //var rot = -10;
            //var transX = -40;

        var cardsPos = [
            null,
            {rot : 0, rotIncrement : 0, transX : 0, transIncrement : 0},
            {rot : 0, rotIncrement : 0, transX : -40, transIncrement : 70},
            {rot : 0, rotIncrement : 0, transX : -80, transIncrement : 70},
            {rot : -20, rotIncrement : 12, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 9, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 7.3, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 6, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 5.2, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 4.6, transX : 0, transIncrement : 0},
            {rot : -20, rotIncrement : 4.1, transX : 0, transIncrement : 0}
        ];       

        for(var i in $scope.selectedCards)
        {
            $scope.selectedCards[i].bitmap.scaleX = 0.3;
            $scope.selectedCards[i].bitmap.scaleY = 0.3;
            $scope.selectedCards[i].enableDrag();

            //note: need to use closure here!
            (function(i){


                $scope.selectedCards[i].bitmap.on("mouseover", function(){
                    $scope.stage.addChild($scope.selectedCards[i].hoverImage);
                });

                $scope.selectedCards[i].bitmap.on("mouseout", function(){
                    $scope.stage.removeChild($scope.selectedCards[i].hoverImage);
                });

                $scope.selectedCards[i].bitmap.image.onload = function(){

                    $scope.selectedCards[i].bitmap.regX = $scope.selectedCards[i].bitmap.image.width/2;
                    $scope.selectedCards[i].bitmap.regY = $scope.selectedCards[i].bitmap.image.height/2;

                    if(numCards > 3)
                    {
                        //$scope.selectedCards[i].bitmap.rotation = cardsPos[numCards].rot + i * cardsPos[numCards].rotIncrement;
                        var angle = cardsPos[numCards].rot + i * cardsPos[numCards].rotIncrement;
                        var result = rotatePoint(new createjs.Point($scope.selectedCards[i].bitmap.x, $scope.selectedCards[i].bitmap.y), new createjs.Point(370,770), angle);
                        $scope.selectedCards[i].bitmap.x = result.x;
                        $scope.selectedCards[i].bitmap.y = result.y;
                        $scope.selectedCards[i].bitmap.rotation = angle;
                    }
                    else
                    {
                        $scope.selectedCards[i].bitmap.x += cardsPos[numCards].transX + i * cardsPos[numCards].transIncrement;
                    }

                    $scope.stage.addChild($scope.selectedCards[i].bitmap);

                };

            })(i);

        }

        createjs.Ticker.addEventListener("tick", function(){

            //make sure the cards are added in order
            for(var i in $scope.selectedCards)
            {
                $scope.stage.removeChild($scope.selectedCards[i].bitmap);
                $scope.stage.addChild($scope.selectedCards[i].bitmap);

                if($scope.selectedCards[i].moving)
                {
                    $scope.stage.removeChild($scope.selectedCards[i].hoverImage);
                }
            }

            $scope.stage.update();
        });
    };
     
});